# E-commerce Microfrontends
## Levantando los proyectos
Una vez clonado el proyecto, entrá a la carpeta raíz y ejecutá los siguientes comandos:

- ```npm run install```
- ```npm run start```

El comando ```install``` puede tardar algunos minutos, ya que instalara las dependencias de los 4 microfrontends y los 2 backends.

El comando ```start``` levantara todos los proyectos.

Ingresando a la url ```http://localhost:8080/``` accedes a la aplicación contenedora que va a integrar todos los proyectos.

## URLs de los frontends y backends

- Contenedora:
    - Frontend http://localhost:8080/
- Productos:
    - Frontend http://localhost:8082/
- Usuario:
    - Frontend http://localhost:8081/
    - Backend http://localhost:4041/api
- Carrito:
    - Frontend http://localhost:8083/
    - Backend http://localhost:4043/api
- Pago:
    - Frontend http://localhost:8084/
    - Backend http://localhost:4044/api