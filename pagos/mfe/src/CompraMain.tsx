import React, { Suspense } from 'react'
import BootstrapForm from './components/BootstrapForm';
import { Usuario } from './types/usuario';
import ErrorBoundary from './components/ErrorBoundary';

import MarcoContenido from 'contenedora/MarcoContenido';
const Carrito = React.lazy(() => import('carrito/CarritoMain'));

import 'bootstrap/dist/css/bootstrap.min.css';
import CarritoCompra from './components/CarritoCompra';

function CompraMain(props: {
    usuario?: Usuario,
    pagar: Function,
    carrito: any,
    setCarrito: Function,
    onClickPagarCompra: Function,
}) {
    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-5 col-lg-6 order-md-last p-0">
                    <MarcoContenido
                        titulo='Contenido del carrito'
                        footer={props.carrito.length === 0
                            ? 'Aun no has comprado productos...'
                            : '¡Aun no completaste esta compra!'}
                        colorMarco='darkseagreen'
                        colorTitulo='seagreen'>
                        <ErrorBoundary element={<CarritoCompra productos={props.carrito} />}>
                            <Suspense fallback={<div>Cargando</div>}>
                                <Carrito productos={props.carrito} setProductos={props.setCarrito} onClickPagarCompra={props.onClickPagarCompra} />
                            </Suspense>
                        </ErrorBoundary>
                    </MarcoContenido>
                </div>
                <div className="col-md-7 col-lg-6 p-0">
                    <MarcoContenido
                        titulo='Formulario de pago'
                        footer='Completá tu compra!'
                        colorMarco='#F5A660'
                        colorTitulo='#B96113'>
                        <BootstrapForm usuario={props.usuario} pagado={props.pagar} />
                    </MarcoContenido>
                </div>
            </div>
        </div>
    );
};

export default CompraMain;