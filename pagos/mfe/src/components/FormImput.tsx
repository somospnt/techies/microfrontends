import React from "react";

import MensajeErrorForm from "./MensajeErrorForm";

import 'bootstrap/dist/css/bootstrap.min.css';

const FormInput = (
    props: {
        id?: string,
        classContainer?: string,
        classLabel?: string
        classInmput?: string,
        labelText?: string,
        name: string,
        value: string | number,
        placeholder?: string,
        type?: 'text' | 'number' | 'email',
        onChange: Function,
        error?: boolean,
        errorMessage?: string,
    }
): JSX.Element => {
    return (
        <div className={props.classContainer || ""}>
            {props.labelText && <label htmlFor={props.name} className={props.classLabel || ""}>{props.labelText}</label>}
            <input
                id={props.id}
                type={props.type || 'text'}
                className={props.classInmput || ""}
                name={props.name}
                placeholder={props.placeholder || ''}
                value={props.value}
                onChange={props.onChange as React.ChangeEventHandler<HTMLInputElement>}
            />
            {props.error &&
                <MensajeErrorForm
                    valorInput={props.value.toString()}
                    mensajeError={props.errorMessage || ''}
                />
            }
        </div>
    );
};

export default FormInput;