import React from 'react'

function CarritoCompra(props: {
    productos: Array<any>
}) {
    const calcularPrecioTotal = () => {
        let suma = 0;
        props.productos.forEach(producto => suma += producto.precio)
        return suma;
    };

    return <>
        <h4 className="d-flex justify-content-between align-items-center mb-3">
            <span className="badge bg-primary rounded-pill">Tu Carrito</span>
            <span className="badge bg-primary rounded-pill">{props.productos.length}</span>
        </h4>
        <ul className="list-group mb-3">
            {props.productos.map(producto => {
                return (
                    <li className="list-group-item d-flex justify-content-between lh-sm">
                        <div>
                            <h6 className="my-0">{producto.nombre}</h6>
                            <small className="text-muted">{producto.descripcion}</small>
                        </div>
                        <span className="text-muted">{`$${producto.precio}`}</span>
                    </li>
                )
            })}
            <li className="list-group-item d-flex justify-content-between">
                <span>Total</span>
                <strong>{`$${calcularPrecioTotal()}`}</strong>
            </li>
        </ul>
    </>
}

export default CarritoCompra;