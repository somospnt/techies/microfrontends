import React from "react";
import Alert from 'react-bootstrap/Alert';

export default class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { error: null, errorInfo: null };
    }

    componentDidCatch(error, errorInfo) {
        // Catch errors in any components below and re-render with error message
        this.setState({
            error: error,
            errorInfo: errorInfo
        })
        // You can also log error messages to an error reporting service here
    }

    render() {
        if (this.state.errorInfo) {
            // Error path
            return this.props.element ?? (
                <Alert variant="danger" >
                    <Alert.Heading>Oh no! Tienes un error!</Alert.Heading>
                    <p>Por el momento esta funcionalidad no está disponible.</p>
                    <p>Vuelve a intentarlo mas tarde</p>
                </Alert>
            );
        }
        // Normally, just render children
        return this.props.children;
    }
}