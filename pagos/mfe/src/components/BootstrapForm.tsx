import React, { useEffect, useState } from 'react'
import { provincias } from '../../../../provincias';
import MensajeErrorForm from './MensajeErrorForm';
import { Usuario } from '../types/usuario';

import * as services from '../services/pagos';

import 'bootstrap/dist/css/bootstrap.min.css';
import FormInput from './FormImput';

function BootstrapForm(props: {
    pagado: Function,
    usuario?: Usuario
}) {
    const [isDebito, setIsDebito] = useState(false);
    const [error, setError] = useState(false);
    const [datos, guardarDatos] = useState({
        nombre: '',
        email: '',
        calle: '',
        codpostal: '',
        provincia: '',
        debitoTitular: '',
        debitoNumero: '',
        debitoVencimiento: '',
        debitoCVV: '',
        creditoTitular: '',
        creditoNumero: '',
        creditoVencimiento: '',
        creditoCVV: ''
    });

    const {
        nombre,
        email,
        calle,
        codpostal,
        provincia,
        debitoTitular,
        debitoNumero,
        debitoVencimiento,
        debitoCVV,
        creditoTitular,
        creditoNumero,
        creditoVencimiento,
        creditoCVV
    } = datos;

    useEffect(() => {
        if (props.usuario) {
            const { credito, debito, direccion, email, nombre } = props.usuario;
            if (credito && debito && direccion) {
                const {
                    direccion: {
                        calle,
                        codpostal,
                        provincia,
                    },
                    credito: {
                        titular: creditoTitular,
                        numero: creditoNumero,
                        vencimiento: creditoVencimiento,
                        CVV: creditoCVV
                    },
                    debito: {
                        titular: debitoTitular,
                        numero: debitoNumero,
                        vencimiento: debitoVencimiento,
                        CVV: debitoCVV
                    }
                } = props.usuario;
                guardarDatos({
                    nombre,
                    email,
                    calle,
                    codpostal,
                    provincia,
                    debitoTitular,
                    debitoNumero,
                    debitoVencimiento,
                    debitoCVV,
                    creditoTitular,
                    creditoNumero,
                    creditoVencimiento,
                    creditoCVV
                });
            }
        }
    }, [props.usuario]);

    const onChange = event => {
        guardarDatos({
            ...datos,
            [event.target.name]: event.target.value
        });
    };

    const onSubmitForm = (event) => {
        event.preventDefault();
        if (validarDatos() || !validarDatosTarjetas()) {
            setError(true);
        } else {
            props.pagado({
                nombre,
                email,
                direccion: {
                    calle,
                    codpostal,
                    provincia,
                },
                credito: {
                    titular: creditoTitular,
                    numero: creditoNumero,
                    vencimiento: creditoVencimiento,
                    CVV: creditoCVV
                },
                debito: {
                    titular: debitoTitular,
                    numero: debitoNumero,
                    vencimiento: debitoVencimiento,
                    CVV: debitoCVV
                }
            });
        }
    };

    const validarDatos = (): boolean => {
        return nombre.trim() === '' ||
            email.trim() === '' ||
            calle.trim() === '' ||
            codpostal.trim() === '' ||
            provincia.trim() === ''
    };
    const validarDatosTarjetas = () => {
        const debitoCargado = debitoTitular.trim() !== '' &&
            debitoNumero.trim() !== '' &&
            debitoVencimiento.trim() !== '' &&
            debitoCVV.trim() !== '';
        const debitoVacio = debitoTitular.trim() === '' &&
            debitoNumero.trim() === '' &&
            debitoVencimiento.trim() === '' &&
            debitoCVV.trim() === '';
        const creditoCargado = creditoTitular.trim() !== '' &&
            creditoNumero.trim() !== '' &&
            creditoVencimiento.trim() !== '' &&
            creditoCVV.trim() !== '';
        const creditoVacio = creditoTitular.trim() === '' &&
            creditoNumero.trim() === '' &&
            creditoVencimiento.trim() === '' &&
            creditoCVV.trim() === '';
        return (creditoCargado && debitoCargado) || (creditoVacio && debitoCargado) || (debitoVacio && creditoCargado);
    };

    return <>
        <h4 className="mb-3">Datos de envios</h4>
        <form className="needs-validation" noValidate onSubmit={onSubmitForm}>
            <div className="row g-3">
                <FormInput classContainer='col-sm-12' classLabel='form-label' classInmput='form-control'
                    labelText='Nombre' name='nombre' value={nombre} type='text' onChange={onChange}
                    error={error} errorMessage='El nombre es requerido.'
                />
                <FormInput classContainer='col-12' classLabel='form-label' classInmput='form-control'
                    labelText='Email' name='email' value={email} type='email' onChange={onChange}
                    error={error} errorMessage='El email es requerido.'
                />

                <FormInput classContainer='col-12' classLabel='form-label' classInmput='form-control'
                    labelText='Dirección de entrega' name='calle' value={calle} type='text' onChange={onChange}
                    error={error} errorMessage='La dirección de entrega es requerida.'
                />

                <div className="col-md-9">
                    <label htmlFor="provincia" className="form-label">Provincia</label>
                    <select className="form-select" name="provincia" value={provincia} onChange={onChange}>
                        <option value="">Seleccionar...</option>
                        {provincias.map(provincia => {
                            return <option key={provincia.id} value={provincia.nombre}>{provincia.nombre}</option>
                        })}
                    </select>
                    {error && <MensajeErrorForm valorInput={provincia} mensajeError='Seleccione una Provincia' />}
                </div>

                <FormInput classContainer='col-md-3' classLabel='form-label' classInmput='form-control'
                    labelText='Dirección de entrega' name='codpostal' value={codpostal} type='number' onChange={onChange}
                    error={error} errorMessage='Ingresá el código postal.'
                />
            </div>

            <hr className="my-4" />

            <h4 className="mb-3">Forma de pago</h4>

            <div className="my-3">
                <div className="form-check">
                    <input id="credit" name="paymentMethod" type="radio" className="form-check-input" checked={!isDebito} onChange={() => setIsDebito(false)} />
                    <label className="form-check-label" htmlFor="credit">Tarjeta de Crédito</label>
                </div>
                <div className="form-check">
                    <input id="debit" name="paymentMethod" type="radio" className="form-check-input" checked={isDebito} onChange={() => setIsDebito(true)} />
                    <label className="form-check-label" htmlFor="debit">Tarjeta de Débito</label>
                </div>
            </div>

            <div className="row gy-3">
                <div className="col-md-6">
                    <label htmlFor={isDebito ? 'debitoTitular' : 'creditoTitular'} className="form-label">Nombre de titular</label>
                    <input type="text" className="form-control" name={isDebito ? 'debitoTitular' : 'creditoTitular'} placeholder="" value={isDebito ? debitoTitular : creditoTitular} onChange={onChange} />
                </div>

                <div className="col-md-6">
                    <label htmlFor={isDebito ? 'debitoNumero' : 'creditoNumero'} className="form-label">Número de tarjeta</label>
                    <input type="text" className="form-control" name={isDebito ? 'debitoNumero' : 'creditoNumero'} placeholder="" value={isDebito ? debitoNumero : creditoNumero} onChange={onChange} />
                </div>

                <div className="col-md-3">
                    <label htmlFor={isDebito ? 'debitoVencimiento' : 'creditoVencimiento'} className="form-label">Vencimiento</label>
                    <input type="text" className="form-control" name={isDebito ? 'debitoVencimiento' : 'creditoVencimiento'} placeholder="" value={isDebito ? debitoVencimiento : creditoVencimiento} onChange={onChange} />
                    <div className="invalid-feedback">
                        Expiration date required
                    </div>
                </div>

                <div className="col-md-3">
                    <label htmlFor={isDebito ? 'debitoCVV' : 'creditoCVV'} className="form-label">CVV</label>
                    <input type="text" className="form-control" name={isDebito ? 'debitoCVV' : 'creditoCVV'} placeholder="" value={isDebito ? debitoCVV : creditoCVV} onChange={onChange} />
                    <div className="invalid-feedback">
                        Security code required
                    </div>
                </div>
            </div>
            <button className="w-100 btn btn-primary btn-lg my-4" type="submit">Pagar</button>
        </form>
    </>
};

export default BootstrapForm;