import React from 'react'

function MensajeErrorForm(props: { valorInput: string, mensajeError: string, funcionValidacion?: Function }) {
    const validar = (): boolean => {
        return props.funcionValidacion?.() || !props.valorInput.trim();
    };
    return <>{
        validar() &&
        <div className="invalid-feedback d-block">
            {props.mensajeError}
        </div>
    }</>
}

export default MensajeErrorForm