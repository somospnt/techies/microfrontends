import React from 'react';
import ReactDOM from 'react-dom/client';
import CompraMain from "./CompraMain";
// import { Usuario } from "./type";

const usuario = {
    _id: 'id',
    nombre: 'El Pepe',
    email: 'elpepe@email.com',
    direccion: {
        provincia: 'Buenos Aires',
        calle: 'Miguelete 201',
        codpostal: '1212',
        _id: 'idDireccion',
    },
    credito: {
        titular: 'El Pepe',
        numero: '1234-1234-1234-1234',
        vencimiento: '01/2031',
        CVV: '123',
        _id: 'idCredito',
    },
    debito: {
        titular: 'El Pepe',
        numero: '5678-5678-5678-5678',
        vencimiento: '02/2032',
        CVV: '456',
        _id: 'idDebito',
    },
    // debito: {
    //     titular: '',
    //     numero: '',
    //     vencimiento: '',
    //     CVV: '',
    //     _id: '',
    // }
}

const carrito = [
    { "id": 2, "nombre": "Auricurales", "descripcion": "Auriculares Logitech H390 negro", "precio": 6000, "imagenUrl": "http://localhost:8082/img/producto (2).jpg", "etiquetas": ["electronica", "audio y video", "audio", "auriculares"], "cantidad": 1 },
    { "id": 3, "nombre": "Remera", "descripcion": "Remera Personalizada 100% Algodón Estampado Dtg Fotos/imagen", "precio": 3800, "imagenUrl": "http://localhost:8082/img/producto (3).jpg", "etiquetas": ["ropa", "accesorios", "remeras", "musculosas", "chombas"], "cantidad": 1 },
    { "id": 1, "nombre": "NAVARRO CORREAS", "descripcion": "Navarro Correas Extra Brut 750ml", "precio": 75, "imagenUrl": "http://localhost:8082/img/producto (1).jpg", "etiquetas": ["bebida", "vino", "espumante", "botella"], "cantidad": 1 }, { "id": 6, "nombre": "Notebook", "descripcion": "Notebook Enova gris 14\", Intel Core i5 8GB de RAM 480GB SSD", "precio": 229, "imagenUrl": "http://localhost:8082/img/producto (6).jpg", "etiquetas": ["computacion", "laptops", "notebooks"], "cantidad": 1 },
    { "id": 5, "nombre": "Aspiradora", "descripcion": "Aspiradora Electrolux Ergorápido ERG21 460ml blanca 100V/240V", "precio": 52, "imagenUrl": "http://localhost:8082/img/producto (5).jpg", "etiquetas": ["electrodomesticos", "hogar", "aspiradoras"], "cantidad": 1 }, { "id": 4, "nombre": "Horno empotrable", "descripcion": "Horno empotrable eléctrico Atma CHE3061M 70L", "precio": 3800, "imagenUrl": "http://localhost:8082/img/producto (4).jpg", "etiquetas": ["electrodomesticos", "horno", "coccion"], "cantidad": 1 }
];


const root: any = ReactDOM.createRoot(document.getElementById('app') as HTMLElement);
root.render(
    <React.StrictMode>
        <CompraMain usuario={usuario} pagar={() => alert('Compraste')} carrito={carrito} setCarrito={() => { }} onClickPagarCompra={() => alert('Pagando')} />
    </React.StrictMode>
);
