import axios, { AxiosError, AxiosResponse } from 'axios';
import { Usuario } from '../types/usuario';

export const pago = async (usuario: Usuario) => {
    try {
        const token = localStorage.getItem('token');
        const response: AxiosResponse = await axios.post(
            `http://localhost:4044/api/pago`, usuario,
            {
                headers: {
                    authorization: `Bearer ${token}`
                }
            });
        return response;
    } catch (error: any) {
        error.origin as unknown as AxiosError;
        throw error;
    };
};

export const ultimo = async (usuario: Usuario) => {
    try {
        const token = localStorage.getItem('token');
        const response: AxiosResponse = await axios.post(
            `http://localhost:4044/api/ultimo/${usuario.email}`,
            {
                headers: {
                    authorization: `Bearer ${token}`
                }
            }
        );
        return response;
    } catch (error: any) {
        error.origin as unknown as AxiosError;
        throw error;
    };
};