export type Usuario = {
    _id: string,
    nombre: string,
    email: string,
    direccion: {
        provincia: string,
        calle: string,
        codpostal: string,
        _id: string,
    },
    credito: {
        titular: string,
        numero: string,
        vencimiento: string,
        CVV: string,
        _id: string,
    },
    debito: {
        titular: string,
        numero: string,
        vencimiento: string,
        CVV: string,
        _id: string,
    }
}