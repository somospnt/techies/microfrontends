import * as services from '../services/services.js';

export const crearPago = async (req, res, next) => {
    try {
        // const token = req.headers.authorization;
        const usuario = req.body;
        const response = await services.crearPago(usuario);
        res.status(200).send(response);
    } catch (error) {
        console.log(error);
    };
    next();
};

export const ultimoPago = async (req, res, next) => {
    try {
        // const token = req.headers.authorization;
        const { usuario } = req.params;
        const response = await services.ultimoPago(usuario);
        res.status(200).send(response);
    } catch (error) {
        console.log(error);
    };
    next();
};

export const actualizarDatos = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        const usuario= req.body;
        const response = await services.actualizarDatos(token, usuario);
        res.status(200).send(response);
    } catch (error) {
        console.log(error);
    };
    next();
};