import express from 'express';
import * as controllers from '../controller/controller.js';

const router = express.Router();

router.post('/pago', controllers.crearPago);
router.get('/ultimo/:usuario', controllers.ultimoPago);
// router.put('/pago', controllers.actualizarDatos);

export default router;