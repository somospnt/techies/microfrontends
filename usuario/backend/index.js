import express from 'express';
import conectarDB from './config/db.js';
import dotenv from 'dotenv';
import usuarioRoutes from './routes/usuarioRoutes.js';
import cors from 'cors';

const app = express();
app.use(express.json());

app.use(cors());

dotenv.config();

conectarDB();

app.use('/api/usuarios', usuarioRoutes);

const PORT = process.env.PORT || 4044;
app.listen(PORT, () => {
    console.log(`Servidor corriendo en el puerto ${PORT}`);
});