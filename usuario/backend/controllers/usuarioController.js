import Usuario from "../models/Usuario.js";
import generarId from "../utils/generarId.js";
import generarJWT from "../utils/generarJWT.js";
import axios from 'axios';

const registrar = async (req, res) => {
    // Evitar usuarios Registrados
    const { email } = req.body;
    const existeUsuario = await Usuario.findOne({ email });

    if (existeUsuario) {
        const error = new Error('Usuario ya registrado');
        return res.status(400).json({ msg: error.message });
    }
    try {
        const usuario = new Usuario(req.body);
        usuario.token = generarId();
        const usuarioAlmacenado = await usuario.save();
        try {
            await axios.post(`http://localhost:4043/api/carrito/${usuarioAlmacenado.email}/crear`);
        } catch (error) {
            console.log('Se rompio el carrito');
        }

        res.json(usuarioAlmacenado);
    } catch (error) {
        console.log(error);
    }
};

const autenticar = async (req, res) => {
    const { email, password } = req.body;
    // Comprobar si el usuario existe
    const usuario = await Usuario.findOne({ email });

    if (!usuario) {
        const error = new Error('El usuario no existe');
        return res.status(404).json({ msg: error.message });
    }
    // Comprobar que el email este confirmado
    if (!usuario.confirmado) {
        const error = new Error('Tu cuenta no ha sido confirmado');
        return res.status(403).json({ msg: error.message });
    }
    // Comprobar password
    if (await usuario.comprobarPassword(password)) {
        res.json({
            _id: usuario._id,
            nombre: usuario.nombre,
            email: usuario.email,
            token: generarJWT(usuario._id),
        })
    } else {
        const error = new Error('El password es incorrecto');
        return res.status(403).json({ msg: error.message });
    }
};

const confirmar = async (req, res) => {
    const { token } = req.params;
    const usuarioConfirmar = await Usuario.findOne({ token });

    if (!usuarioConfirmar) {
        const error = new Error('Token no válido');
        return res.status(403).json({ msg: error.message });
    }

    try {
        usuarioConfirmar.confirmado = true;
        usuarioConfirmar.token = '';
        await usuarioConfirmar.save();
        res.json({ msg: 'Usuario Confirmado Correctamente' });
    } catch (error) {
        console.log(error);
    }
};

const perfil = async (req, res) => {
    const { usuario } = req;
    res.json(usuario);
};

const modificar = async (req, res) => {
    const usuario = await Usuario.findOne({ _id: req.usuario._id });
    const { direccion, debito, credito } = req.body;
    try {
        if (debito) {
            usuario.debito = debito;
        }
        if (direccion) {
            usuario.direccion = direccion;
        }
        if (credito) {
            usuario.credito = credito;
        }
        await usuario.save();
        res.json({
            _id: usuario._id,
            nombre: usuario.nombre,
            email: usuario.email,
            debito: usuario.debito,
            credito: usuario.credito,
            direccion: usuario.direccion
        })
    } catch (error) {
        console.log(error);
    }
};

export { registrar, autenticar, confirmar, perfil, modificar };