import express from 'express';
const router = express.Router();
import { registrar, autenticar, confirmar, perfil, modificar } from '../controllers/usuarioController.js';
import checkAuth from '../middleware/checkAuth.js';

router.post('/', registrar);
router.post('/login', autenticar);
router.get('/confirmar/:token', confirmar);
router.put('/datos-personales', checkAuth, modificar);
router.get('/perfil', checkAuth, perfil);

export default router;