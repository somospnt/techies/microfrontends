import mongoose from "mongoose";

const direccionSchema = mongoose.Schema({
    provincia: {
        type: String,
        require: true,
        trim: true
    },
    calle: {
        type: String,
        require: true,
        trim: true
    },
    codpostal: {
        type: String,
        require: true,
        trim: true,
    },
});

const debitoSchema = mongoose.Schema({
    titular: {
        type: String,
        require: true,
        trim: true
    },
    numero: {
        type: String,
        require: true,
        trim: true
    },
    vencimiento: {
        type: String,
        require: true,
        trim: true
    },
    CVV: {
        type: String,
        require: true,
        trim: true
    },
});

const creditoSchema = mongoose.Schema({
    titular: {
        type: String,
        require: true,
        trim: true
    },
    numero: {
        type: String,
        require: true,
        trim: true
    },
    vencimiento: {
        type: String,
        require: true,
        trim: true
    },
    CVV: {
        type: String,
        require: true,
        trim: true
    },
});

export { direccionSchema, creditoSchema, debitoSchema };