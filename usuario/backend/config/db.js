import mongoose from "mongoose";

const conectarDB = async () => {
    try {
        const mdbUser = process.env.DB_USER_NAME;
        const mdbPass = process.env.DB_USER_PASSWORD;
        const mdbCluster = process.env.DB_CLUSTER_NAME;
        const mdbClusterUrl = process.env.DB_CLUSTER_URL;
        const mdbDbName = process.env.DB_NAME;
        const mdbOptions = process.env.DB_OPTIONS;
        const mdbUrl = 'mongodb+srv://' + mdbUser + ':' + mdbPass + '@' + mdbCluster + '.' + mdbClusterUrl + '/' + mdbDbName + '?' + mdbOptions;
        const connection = await mongoose.connect(mdbUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        const url = `${connection.connection.host}:${connection.connection.port}`;
        console.log(`MongoDB Conectado en: ${url}`);
    } catch (error) {
        console.log(`error: ${error.menssage}`);
        process.exit(1);
    }
};

export default conectarDB;