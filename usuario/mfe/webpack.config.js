const HtmlWebPackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const deps = require("./package.json").dependencies;
module.exports = {
  output: {
    publicPath: "http://localhost:8081/",
  },

  resolve: {
    extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
  },

  devServer: {
    port: 8081,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.m?js/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(css|s[ac]ss)$/i,
        use: ["style-loader", "css-loader", "postcss-loader"],
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.js$/,
        enforce: 'pre',
        use: ['source-map-loader'],
      },
    ],
  },

  plugins: [
    new ModuleFederationPlugin({
      name: "usuario",
      filename: "remoteEntry.js",
      remotes: {},
      exposes: {
        './Login': './src/components/Login.tsx',
        './NuevaCuenta': './src/components/NuevaCuenta.tsx',
        './DatosPersonales': './src/components/DatosPersonales.tsx',
        './Tarjetas': './src/components/Tarjetas.tsx',
        './IconoDatosPersonales': './src/components/IconoDatosPersonales.tsx',
        './CerrarSesionBoton': './src/components/CerrarSesionBoton.tsx',
        './BotonUsuario': './src/components/BotonUsuario.tsx',
        './AuthState': './src/context/autenticacion/authState.js',
        './AuthContext': './src/context/autenticacion/authContext.js',
        './AuthReducer': './src/context/autenticacion/authReducer.js',
        './AlertaState': './src/context/alerta/alertaState.js',
        './getUsuario': './src/service/usuario.ts',
      },
      shared: {
        ...deps,
        react: {
          singleton: true,
          requiredVersion: deps.react,
        },
        "react-dom": {
          singleton: true,
          requiredVersion: deps["react-dom"],
        },
      },
    }),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
    }),
  ],
};
