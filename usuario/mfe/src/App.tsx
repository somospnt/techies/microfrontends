import React from 'react';
import ReactDOM from 'react-dom/client';
import Usuario from './Usuario';

const root: any = ReactDOM.createRoot(document.getElementById('app') as HTMLElement);
root.render(
  <React.StrictMode>
    <Usuario />
  </React.StrictMode>
);