import axios from "axios";

const clienteAxios = axios.create({
    baseURL: 'http://localhost:4041'
});

export default clienteAxios;