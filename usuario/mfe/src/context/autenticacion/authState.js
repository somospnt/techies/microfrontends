import React, { useReducer } from "react";
import AuthReducer from "./authReducer";
import AuthContext from './authContext';
import clienteAxios from '../../config/axios';
import tokenAuth from '../../config/token';

import {
    REGISTRO_ERROR,
    OBTENER_USUARIO,
    LOGIN_EXISTOSO,
    LOGIN_ERROR,
    CERRAR_SESION
} from "../../types";

const AuthState = props => {
    const initialState = {
        token: localStorage.getItem('token'),
        autenticado: null,
        usuario: null,
        mensaje: null,
        cargando: true
    }

    const [state, dispatch] = useReducer(AuthReducer, initialState);

    const registrarUsuario = async datos => {
        try {
            await clienteAxios.post('api/usuarios', datos);
            iniciarSesion(datos);

            // Obtener el usuario
            await usuarioAutenticado();
        } catch (error) {
            const alerta = {
                msg: error.response.data.msg,
                categoria: 'alerta-error'
            }
            dispatch({
                type: REGISTRO_ERROR,
                payload: alerta
            })
        }
    };

    const usuarioAutenticado = async () => {
        try {
            const token = localStorage.getItem('token');
            if (token) {
                tokenAuth(token);
            } else {
                throw new Error('Usuario no logueado');
            }

            const respuesta = await clienteAxios.get('/api/usuarios/perfil');
            dispatch({
                type: OBTENER_USUARIO,
                payload: respuesta.data
            })
        } catch (error) {
            dispatch({
                type: LOGIN_ERROR
            })
        }
    };

    // Cuando el usuario inicia sesion
    const iniciarSesion = async datos => {
        try {
            const respuesta = await clienteAxios.post('/api/usuarios/login', datos);
            localStorage.setItem('token', respuesta.data.token);
            dispatch({
                type: LOGIN_EXISTOSO,
                payload: respuesta.data
            });

            // Obtener el usuario
            await usuarioAutenticado();
        } catch (error) {
            const alerta = {
                msg: error.response.data.msg,
                categoria: 'alerta-error'
            }
            dispatch({
                type: LOGIN_ERROR,
                payload: alerta
            })
        }
    };

    const actualizar = async datos => {
        try {
            const respuesta = await clienteAxios.put('/api/usuarios/datos-personales', datos);

            // Obtener el usuario
            await usuarioAutenticado();
        } catch (error) {
            const alerta = {
                msg: error.response.data.msg,
                categoria: 'alerta-error'
            }
            dispatch({
                type: LOGIN_ERROR,
                payload: alerta
            })
        }
    };

    // Cierra la sesion del usuario
    const cerrarSesion = () => {
        dispatch({
            type: CERRAR_SESION
        });
    }

    return (
        <AuthContext.Provider
            value={{
                token: state.token,
                autenticado: state.autenticado,
                usuario: state.usuario,
                mensaje: state.mensaje,
                cargando: state.cargando,
                registrarUsuario,
                usuarioAutenticado,
                iniciarSesion,
                cerrarSesion,
                actualizar
            }}
        >
            {props.children}
        </AuthContext.Provider>
    )
}

export default AuthState;