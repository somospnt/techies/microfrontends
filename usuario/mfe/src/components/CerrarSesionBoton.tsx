import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../context/autenticacion/authContext';

import '../index.css';

function CerrarSesionBoton(props: {
    onClick: Function
}) {
    const { autenticado, cerrarSesion, usuarioAutenticado } = useContext(AuthContext);

    useEffect(() => {
        if (!autenticado) {
            usuarioAutenticado();
        }
    }, [autenticado]);

    const onClick = () => {
        props.onClick();
        cerrarSesion();
    };

    return (<>{
        autenticado &&
        <Link
            to='datos-personales'
            className='link'
            onClick={onClick}
        >
            Cerrar Sesion
        </Link>
    }</>)
}

export default CerrarSesionBoton;