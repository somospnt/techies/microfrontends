import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from 'react-router-dom';
import AlertaContext from '../context/alerta/alertaContext';
import AuthContext from "../context/autenticacion/authContext";
import '../index.css';

const Tarjetas = (props: { pathToRedirect: string }) => {

    const navigate = useNavigate();
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta } = alertaContext;

    const authContext = useContext(AuthContext);
    const { mensaje, autenticado, actualizar, usuario } = authContext;

    const [redirigir, setRedirigir] = useState(false);
    const [hayCambios, setHayCambios] = useState(false);

    useEffect(() => {
        if (usuario) {
            const { credito, debito } = usuario;
            if (credito && debito) {
                const {
                    credito: {
                        titular: creditoTitular,
                        numero: creditoNumero,
                        vencimiento: creditoVencimiento,
                        CVV: creditoCVV
                    },
                    debito: {
                        titular: debitoTitular,
                        numero: debitoNumero,
                        vencimiento: debitoVencimiento,
                        CVV: debitoCVV
                    }
                } = usuario;
                guardarTarjetas({
                    creditoTitular,
                    creditoNumero,
                    creditoVencimiento,
                    creditoCVV,
                    debitoTitular,
                    debitoNumero,
                    debitoVencimiento,
                    debitoCVV,
                });
            }
        }
    }, [usuario]);

    useEffect(() => {
        if (!autenticado) {
            return navigate('/');
        }
        if (redirigir) {
            return navigate(props.pathToRedirect);
        }
        if (mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria);
        }
        // eslint-disable-next-line
    }, [mensaje, redirigir, autenticado]);
    const [isDebito, setIsDebito] = useState(true);

    const [tarjetas, guardarTarjetas] = useState({
        debitoTitular: '',
        debitoNumero: '',
        debitoVencimiento: '',
        debitoCVV: '',
        creditoTitular: '',
        creditoNumero: '',
        creditoVencimiento: '',
        creditoCVV: ''
    });

    const {
        debitoTitular,
        debitoNumero,
        debitoVencimiento,
        debitoCVV,
        creditoTitular,
        creditoNumero,
        creditoVencimiento,
        creditoCVV
    } = tarjetas;

    const onChange = event => {
        setHayCambios(true);
        guardarTarjetas({
            ...tarjetas,
            [event.target.name]: event.target.value
        });
    };

    const onSubmit = async event => {
        event.preventDefault();
        if (validarDatos()) {
            if (hayCambios) {
                await actualizar({
                    debito: {
                        titular: debitoTitular,
                        numero: debitoNumero,
                        vencimiento: debitoVencimiento,
                        CVV: debitoCVV
                    },
                    credito: {
                        titular: creditoTitular,
                        numero: creditoNumero,
                        vencimiento: creditoVencimiento,
                        CVV: creditoCVV
                    }
                })
            }
            setRedirigir(true);
        } else {
            mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
        }
    }

    const validarDatos = () => {
        const debitoCargado = debitoTitular.trim() !== '' &&
            debitoNumero.trim() !== '' &&
            debitoVencimiento.trim() !== '' &&
            debitoCVV.trim() !== '';
        const debitoVacio = debitoTitular.trim() === '' &&
            debitoNumero.trim() === '' &&
            debitoVencimiento.trim() === '' &&
            debitoCVV.trim() === '';
        const creditoCargado = creditoTitular.trim() !== '' &&
            creditoNumero.trim() !== '' &&
            creditoVencimiento.trim() !== '' &&
            creditoCVV.trim() !== '';
        const creditoVacio = creditoTitular.trim() === '' &&
            creditoNumero.trim() === '' &&
            creditoVencimiento.trim() === '' &&
            creditoCVV.trim() === '';
        return (creditoCargado && debitoCargado) || (creditoVacio && debitoCargado) || (debitoVacio && creditoCargado);
    };

    return (
        <div className='form-usuario'>
            {alerta ? (<div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>) : null}
            <div className='contenedor-form sombra-dark'>
                <h1 className='titulo'>{isDebito ? 'Tarjeta Debito' : 'Tarjeta Crédito'}</h1>
                <div>
                    <label>
                        Tarjeta debito
                        <input
                            name="isGoing"
                            type="checkbox"
                            checked={isDebito}
                            onChange={() => setIsDebito(true)} />
                    </label>
                    <label>
                        Tarjeta credito
                        <input
                            name="isGoing"
                            type="checkbox"
                            checked={!isDebito}
                            onChange={() => setIsDebito(false)} />
                    </label>
                </div>

                <form
                    onSubmit={onSubmit}
                >
                    {isDebito ?
                        <>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>Titular</label>
                                <input
                                    type='text'
                                    id='debitoTitular'
                                    name='debitoTitular'
                                    placeholder='Nombre en tarjeta'
                                    value={debitoTitular}
                                    onChange={onChange}
                                />
                            </div>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>Número</label>
                                <input
                                    type='text'
                                    id='debitoNumero'
                                    name='debitoNumero'
                                    placeholder='XXXX-XXXX-XXXX-XXXX'
                                    value={debitoNumero}
                                    onChange={onChange}
                                />
                            </div>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>Vencimiento</label>
                                <input
                                    type='text'
                                    id='debitoVencimiento'
                                    name='debitoVencimiento'
                                    placeholder='mm/aaaa'
                                    value={debitoVencimiento}
                                    onChange={onChange}
                                />
                            </div>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>CVV</label>
                                <input
                                    type='text'
                                    id='debitoCVV'
                                    name='debitoCVV'
                                    placeholder='123'
                                    value={debitoCVV}
                                    onChange={onChange}
                                />
                            </div>
                        </>
                        :
                        <>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>Titular</label>
                                <input
                                    type='text'
                                    id='creditoTitular'
                                    name='creditoTitular'
                                    placeholder='Nombre en tarjeta'
                                    value={creditoTitular}
                                    onChange={onChange}
                                />
                            </div>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>Número</label>
                                <input
                                    type='text'
                                    id='creditoNumero'
                                    name='creditoNumero'
                                    placeholder='XXXX-XXXX-XXXX-XXXX'
                                    value={creditoNumero}
                                    onChange={onChange}
                                />
                            </div>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>Vencimiento</label>
                                <input
                                    type='text'
                                    id='creditoVencimiento'
                                    name='creditoVencimiento'
                                    placeholder='mm/aaaa'
                                    value={creditoVencimiento}
                                    onChange={onChange}
                                />
                            </div>
                            <div className='campo-form'>
                                <label htmlFor='nombre'>CVV</label>
                                <input
                                    type='text'
                                    id='creditoCVV'
                                    name='creditoCVV'
                                    placeholder='123'
                                    value={creditoCVV}
                                    onChange={onChange}
                                />
                            </div>
                        </>
                    }
                    <div className='campo-form'>
                        <input type='submit' className='boton-usuario btn-primario btn-block'
                            value='Confirmar' />
                    </div>
                </form>
                <Link to={'/'} className='enlace-cuenta'>
                    Volver a la Home
                </Link>
            </div>
        </div>
    );
};

export default Tarjetas;