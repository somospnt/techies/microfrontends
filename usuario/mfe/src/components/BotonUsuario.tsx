import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AuthContext from '../context/autenticacion/authContext';

import '../index.css';

function BotonUsuario(props: {
    texto: string,
    path: string
}) {
    const { autenticado, usuarioAutenticado } = useContext(AuthContext);

    useEffect(() => {
        if (!autenticado) {
            usuarioAutenticado();
        }
    }, [autenticado]);

    return (<>{
        !autenticado &&
        <Link
            to={props.path}
            className='link'
        >
            {props.texto}
        </Link>
    }</>)
}

export default BotonUsuario;