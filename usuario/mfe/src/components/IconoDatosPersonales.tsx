import React, { useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthContext from '../context/autenticacion/authContext';

import '../index.css';

function IconoDatosPersonales(props: {
    size?: number,
    color?: string,
}
) {
    const { autenticado, usuarioAutenticado } = useContext(AuthContext);
    useEffect(() => {
        if (!autenticado) {
            usuarioAutenticado();
        }
    }, [autenticado]);
    const size = props?.size || 32;
    const svgColor = props?.color || 'currentColor';
    const navigate = useNavigate();
    const onClick = () => {
        navigate('datos-personales');
    };

    return (
        <>
            {
                autenticado &&
                <button
                    className='icono'
                    onClick={onClick}
                >
                    <svg xmlns="http://www.w3.org/2000/svg" width={size} height={size} fill={svgColor} viewBox="0 0 16 16">
                        <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z" />
                    </svg>
                </button>
            }
        </>
    )
}

export default IconoDatosPersonales;