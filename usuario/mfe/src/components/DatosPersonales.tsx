import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from 'react-router-dom';
import { provincias } from "../../../../provincias";
import AlertaContext from '../context/alerta/alertaContext';
import AuthContext from "../context/autenticacion/authContext";
import '../index.css';

const DatosPersonales = (props: { pathToRedirect: string }) => {

    const navigate = useNavigate();
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta } = alertaContext;

    const { mensaje, actualizar, usuario, autenticado, usuarioAutenticado } = useContext(AuthContext);
    const [redirigir, setRedirigir] = useState(false);
    const [hayCambios, setHayCambios] = useState(false);
    const [direccion, guardarDireccion] = useState({
        calle: '',
        codpostal: '',
        provincia: ''
    });

    const {
        provincia,
        calle,
        codpostal,
    } = direccion;

    useEffect(() => {
        if (!autenticado) {
            usuarioAutenticado();
        }
    }, [autenticado]);

    useEffect(() => {
        if (usuario) {
            const { direccion } = usuario;
            if (direccion) {
                guardarDireccion({ ...direccion })
            }
        }
    }, [usuario]);

    useEffect(() => {
        if (!autenticado) {
            return navigate('/');
        }
        if (redirigir) {
            return navigate(props.pathToRedirect);
        }
        if (mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria);
        }
        // eslint-disable-next-line
    }, [mensaje, redirigir]);



    const onChange = event => {
        setHayCambios(true);
        guardarDireccion({
            ...direccion,
            [event.target.name]: event.target.value
        });
    };

    const onSubmit = async event => {
        event.preventDefault();
        // Validar que no haya campos vacios
        if (
            calle.trim() === '' ||
            codpostal.trim() === '' ||
            provincia.trim() === ''
        ) {
            mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
            return;
        }
        if (hayCambios) {
            // Pasarlo al action
            await actualizar({
                direccion: {
                    calle,
                    provincia,
                    codpostal
                },
            });
        }
        setRedirigir(true);
    }
    return (
        <div className='form-usuario'>
            {alerta ? (<div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>) : null}
            <div className='contenedor-form sombra-dark'>
                <h1 className='titulo'>Datos personales</h1>
                <form
                    onSubmit={onSubmit}
                >
                    <div className='campo-form '>
                        <label>Provincia</label>
                        <select name="provincia" value={provincia} onChange={onChange}>
                            <option value={'Seleccionar...'}>Seleccionar...</option>
                            {provincias.map(provincia => {
                                return <option key={provincia.id} value={provincia.nombre}>{provincia.nombre}</option>
                            })}
                        </select>
                    </div>
                    <div className='campo-form'>
                        <label htmlFor='nombre'>Dirección</label>
                        <input
                            type='text'
                            id='direccion'
                            name='calle'
                            placeholder='Tu Calle'
                            value={calle}
                            onChange={onChange}
                        />
                    </div>
                    <div className='campo-form'>
                        <label htmlFor='email'>Cód Postal</label>
                        <input
                            type='number'
                            id='text'
                            max={9999}
                            name='codpostal'
                            placeholder='Tu Código Postal'
                            value={codpostal}
                            onChange={onChange}
                        />
                    </div>
                    <div className='campo-form'>
                        <input type='submit' className='boton-usuario btn-primario btn-block'
                            value='Siguiente' />
                    </div>
                </form>
                <Link to={'/'} className='enlace-cuenta'>
                    Volver a la Home
                </Link>
            </div>
        </div>
    );
};

export default DatosPersonales;