import clienteAxios from "../config/axios";
import tokenAuth from "../config/token";

export async function getUsuario() {
    const token = localStorage.getItem('token');
    if (token) {
        tokenAuth(token);
    } else {
        throw new Error('Usuario no logueado');
    }
    const { data } = await clienteAxios.get('/api/usuarios/perfil')
    return data
}