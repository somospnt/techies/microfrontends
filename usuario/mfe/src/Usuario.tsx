import React from 'react'

import { BrowserRouter, Route, Routes } from "react-router-dom";
import Carrito from "./components/Carrito";
import DatosPersonales from './components/DatosPersonales';
import Login from "./components/Login";
import NuevaCuenta from "./components/NuevaCuenta";
import Tarjetas from './components/Tarjetas';
import AlertaState from "./context/alerta/alertaState";
import AuthState from "./context/autenticacion/authState";
import './index.css';

function Usuario() {
    return (
        <AlertaState>
            <AuthState>
                <BrowserRouter>
                    <Routes>
                        <Route path='/' element={<Login pathToRedirect='/datos-personales' />} />
                        <Route path='/login' element={<Login pathToRedirect='/datos-personales' />} />
                        <Route path='nueva-cuenta' element={<NuevaCuenta pathToRedirect='/datos-personales' />} />
                        <Route path='carrito' element={<Carrito />} />
                        <Route path='datos-personales' element={<DatosPersonales pathToRedirect='/tarjetas' />} />
                        <Route path='tarjetas' element={<Tarjetas pathToRedirect='/carrito' />} />
                    </Routes>
                </BrowserRouter>
            </AuthState>
        </AlertaState>
    )
}

export default Usuario