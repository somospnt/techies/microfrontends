/* Import del plugin de webpack que se va a utilizar para definir el archivo html donde
   se renderizará la aplicación. */
const HtmlWebPackPlugin = require("html-webpack-plugin");
/* Import del plugin de ModuleFederation que contiene la configuración para consumir
   módulos de otros microfrontends y exponer los propios. */
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

/* Se importan las dependencias del proyecto para que al importar desde otro
   microfrontend uno de los módulos expuestos en este archivo, pueda utilizarlo y
   agregarlo a sus dependencias y añadirlas a su código. */
const deps = require("./package.json").dependencies;

/* Aqui esta la configuración que va a utilizar webpack para levantar el servidor y
   exponer sus recursos */
module.exports = {
    output: {
        publicPath: "http://localhost:8083/", //url y puerto del servidor
    },

    resolve: {
        extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
    },

    devServer: {
        //Puero a utilizar cuando se ejecuta el servidor en modo desarrollo
        port: 8083,
        //Si al utilizar la api de historial del navegador retorna 404, se suplanta por el idenx.html
        historyApiFallback: true,
    },

    module: {
        rules: [
            {
                // Regla para que resuelva las importaciones de módulos de ECMAScript y javascript
                // y pueda interpretar su contenido. En este caso no requiere de un loader en particular
                test: /\.m?js/,
                type: "javascript/auto",
                resolve: {
                    fullySpecified: false,
                },
            },
            {
                //loaders para que webpack pueda cargar archivos css
                test: /\.(css|s[ac]ss)$/i,
                use: ["style-loader", "css-loader", "postcss-loader"],
            },
            {
                //loader para que webpack pueda cargar archivos de imágenes
                test: /\.(svg|png|jpg|jpeg|gif|ico)$/,
                use: ["file-loader"],
            },
            {
                //loader para que webpack cargar archivos de javascript/typescript
                test: /\.(ts|tsx|js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                },
            },
        ],
    },

    plugins: [
        new ModuleFederationPlugin({
            name: "mimicrofrontend", //nombre "raiz" con el que se expone esta app para acceder a los modulos que expone: carrito/xyz
            filename: "remoteEntry.js", //entry-point que expone el plugin para acceder a esta app
            remotes: {
                //Una lista de todas las apps remotas que va a consumir esta aplicacion
                otromicrofrontend: "otromicrofrontend@http://otromicrofrontend:8081/remoteEntry.js",
                // Y todos los microfrontends que se necesiten
            },
            exposes: {
                //Una lista de todos los modulos/archivos que va a exponer esta aplicacion
                './MiComponente': './src/MiComponente.tsx',
                './miFuncion': './src/miFuncion.js',
            },
            shared: {
                ...deps, //dependencias del archivo package.json de esta aplicacion
                /*  Estas depencias van a sobreescribir las compartidas en la linea 78 para agregarle configuraciones especificas
                    para React, ya que React necesita tener una unica instancia en la que va a compartir multiples datos a nivel 
                    de librerias de React */
                react: {
                    singleton: true,
                    requiredVersion: deps.react,
                },
                "react-dom": {
                    singleton: true,
                    requiredVersion: deps["react-dom"],
                },
            },
        }),
        new HtmlWebPackPlugin({
            template: "./src/index.html", //archivo donde se va a renderizar esta aplicacion para exponerse
        }),
    ],
};