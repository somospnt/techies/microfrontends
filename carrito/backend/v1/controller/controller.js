import * as services from '../services/services.js';

export const crearCarrito = async (req, res, next) => {
    try {
        const { usuario } = req.params;
        const response = await services.crearCarrito(usuario);
        res.status(201).send(response);
    } catch (error) {
        console.log(error);
    };
    next();
};

export const agregarQuitarProductos = async (req, res, next) => {
    const { usuario } = req.params;
    const carrito = await services.getCarrito(usuario);
    const { producto } = req.body;
    if (producto.cantidad > 0) {
        const indice = carrito.lista.findIndex(item => item.id === producto.id);
        if (indice > -1) {
            carrito.lista[indice].cantidad = producto.cantidad;
        } else {
            carrito.lista.push(producto);
        };
    } else {
        const indice = carrito.lista.findIndex(item => item.id === producto.id);
        if (indice > -1) {
            carrito.lista.splice(indice, 1);
        };
    };
    setCarrito(req, res, next);
};

export const getCarrito = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        const response = await services.getCarrito(token);
        res.status(200).send(response);
    } catch (error) {
        console.log(error);
    };
    next();
};

export const setCarrito = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        const carritoNuevo = req.body;
        const response = await services.setCarrito(token, carritoNuevo);
        res.status(200).send(response);
    } catch (error) {
        console.log(error);
    };
    next();
};