import express, { json } from 'express';
import cors from 'cors';
import productoRouter from '../routes/routes.js';
import { mdbSetAtlasEnvironmentConfig } from '../repository/database.js';

const PATH = process.env.API_PATH;

mdbSetAtlasEnvironmentConfig();

const server = express();
server.use(cors());
server.use(json());

server.get('/', (req, res) => {
    res.status(200).send(`Carrito backend service running!`);
});

server.use(`${PATH}`, productoRouter);

export default server;