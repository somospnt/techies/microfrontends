import { dbAccess } from '../repository/database.js';
import { logResponse } from '../utils/functions.js';
import axios from 'axios';

export const crearCarrito = async (usuario) => {
    const collection = 'carrito';
    const documents = {
        usuario: usuario,
        lista: [],
    };
    const dbResponse = await dbAccess('c', collection, documents);
    console.log(logResponse(collection, documents));
    console.log('Response DB:\n', dbResponse);
    return dbResponse;
};

export const getCarrito = async (token) => {
    try {
        const { data: usuario } = await axios.get(
            'http://localhost:4041/api/usuarios/perfil',
            {
                headers: {
                    Authorization: token
                }
            });
        const collection = 'carrito';
        const response = await dbAccess('r', collection, { usuario: usuario.email });
        return response[0].lista;
    } catch (error) {
        return [];
    }
};

export const setCarrito = async (token, nuevoCarrito) => {

    try {
        const { data: usuario } = await axios.get(
            'http://localhost:4041/api/usuarios/perfil',
            {
                headers: {
                    Authorization: token
                }
            });
        const collection = 'carrito';
        const documents = {
            usuario: usuario.email,
            lista: nuevoCarrito,
        };
        const response = await dbAccess('u', collection, { usuario: usuario.email }, { $set: documents });
        return response;
    } catch (error) {
        return [];
    }
};