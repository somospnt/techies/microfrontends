import express from 'express';
import * as controllers from '../controller/controller.js';

const router = express.Router();

router.post('/carrito/:usuario/crear', controllers.crearCarrito);
router.put('/carrito/:usuario/producto', controllers.agregarQuitarProductos);
router.get('/carrito', controllers.getCarrito);
router.put('/carrito', controllers.setCarrito);

export default router;
