import { environment as env } from './configuration/environment.js';
import server from './server/server.js';

const PORT = env.PORT;
const HOST = env.HOST;
const PATH =  env.API_PATH;

server.listen(PORT, HOST, function () {
    console.log(`Carrito backend listening on http://${HOST}:${PORT}${PATH}`);
});