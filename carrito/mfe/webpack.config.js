const HtmlWebPackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const deps = require("./package.json").dependencies;
module.exports = {
    output: {
        publicPath: "http://localhost:8083/",
    },

    resolve: {
        extensions: [".tsx", ".ts", ".jsx", ".js", ".json"],
    },

    devServer: {
        port: 8083,
        historyApiFallback: true,
    },

    module: {
        rules: [
            {
                test: /\.m?js/,
                type: "javascript/auto",
                resolve: {
                    fullySpecified: false,
                },
            },
            {
                test: /\.(css|s[ac]ss)$/i,
                use: ["style-loader", "css-loader", "postcss-loader"],
            },
            {
                test: /\.(svg|png|jpg|jpeg|gif|ico)$/,
                use: ["file-loader"],
            },
            {
                test: /\.(ts|tsx|js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.js$/,
                enforce: 'pre',
                use: ['source-map-loader'],
            },
        ],
    },

    plugins: [
        new ModuleFederationPlugin({
            name: "carrito",
            filename: "remoteEntry.js",
            remotes: {
                productos: "productos@http://localhost:8082/remoteEntry.js",
            },
            exposes: {
                './CarritoMain': './src/CarritoMain.tsx',
                './CarritoChico': './src/components/CarritoChico.tsx',
                './FooterBotones': './src/components/FooterBotones.tsx',
                './ProductoCard': './src/components/ProductoCard.tsx',
                './ProductoCardTicket': './src/components/ProductoCardTicket.tsx',
                './ProductosEnCarrito': './src/components/ProductosEnCarrito.tsx',
                './useGetCarrito': './src/hooks/useGetCarrito.ts',
                './funcionesDeCarrito': './src/utils.ts',
                './MiniCarrito': './src/components/MiniCarrito.tsx',
                './VistaPreviaCarrito': './src/components/VistaPreviaCarrito.tsx',
                './BotonCarrito': './src/components/BotonCarrito.tsx',
            },
            shared: {
                ...deps,
                react: {
                    singleton: true,
                    requiredVersion: deps.react,
                },
                "react-dom": {
                    singleton: true,
                    requiredVersion: deps["react-dom"],
                },
            },
        }),
        new HtmlWebPackPlugin({
            template: "./src/index.html",
        }),
    ],
};