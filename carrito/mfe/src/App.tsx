import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import { ProductoCarrito } from './App.types';

import Carrito from './CarritoMain';

function App() {
    const [productos, setProductos] = useState<Array<ProductoCarrito>>([]);
    return (
        <Carrito productos={productos} setProductos={setProductos} />
    )
};

const root: ReactDOM.Root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>
);