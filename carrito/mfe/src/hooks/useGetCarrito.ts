import carritoServices from '../services/carrito-services';
import { useState, useEffect } from 'react';
import { ProductoCarrito } from '../App.types';

export type HookGetCarritoType = {
    loading: boolean,
    error: any,
    data: Array<ProductoCarrito>
};

const useGetCarrito = () => {

    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<any>(null);
    const [data, setData] = useState<Array<ProductoCarrito>>([]);

    const llamarAlService = async () => {
        try {
            const response = await carritoServices.getCarrito()
            setData(response);
            setError(null);
            setLoading(false);
        } catch (error) {
            setData([]);
            setError(error);
            setLoading(false);
        }
    };

    useEffect(() => {
        const carrigoStorage = localStorage.getItem('carrito');
        if (!carrigoStorage) {
            if (loading) {
                llamarAlService()
            };
        } else {
            setData(JSON.parse(carrigoStorage));
            setError(null);
            setLoading(false);
        }
    }, [loading]);

    return { loading, data, error };
};

export default useGetCarrito;