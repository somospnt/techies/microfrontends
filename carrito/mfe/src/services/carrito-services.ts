import axios, { AxiosError, AxiosResponse } from 'axios';

import { ProductoCarrito } from '../App.types';

const carritoServices = {
    getCarrito: async () => {
        try {
            const token = localStorage.getItem('token');
            if (!token) {
                throw new Error('Usuario deslogueado');
            }
            const response: AxiosResponse = await axios.get(
                `http://localhost:4043/api/carrito`,
                {
                    headers: {
                        authorization: `Bearer ${token}`
                    }
                });
            return response.data as Array<ProductoCarrito>;
        } catch (error: any) {
            error.origin as unknown as AxiosError;
            throw error;
        };
    },
};

export default carritoServices;