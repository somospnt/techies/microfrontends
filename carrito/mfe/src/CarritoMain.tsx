import React, { useEffect } from 'react';
import ProductosEnCarrito from './components/ProductosEnCarrito';
import FooterBotones from './components/FooterBotones';
import { ProductoCarrito } from './App.types';
import useGetCarrito from './hooks/useGetCarrito';
import { setCarrito } from './utils';

import './styles/Carrito.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const App = (
    props: {
        vistaReducida?: boolean,
        productos: Array<ProductoCarrito>,
        setProductos: Function,
        textoTituloDelMarco?: string,
        textoFooterDelMarco?: string,
        onClickPagarCompra?: Function,
        onClickAgregarMasProductos?: Function
    }
) => {
    const { loading, error, data } = useGetCarrito();

    useEffect(() => {
        if (!loading && !error) {
            setCarrito(data);
            props.setProductos(data);
        };
    }, [loading]);

    const doRender: boolean = !loading && !error;
    const vistaReducida: boolean = props.vistaReducida ?? false;

    return (
        <>
            {doRender && <>
                <ProductosEnCarrito productos={props.productos} setProductos={props.setProductos} vistaReducida={vistaReducida} />
                {!vistaReducida && <FooterBotones
                    productos={props.productos}
                    setProductos={props.setProductos}
                    onClickPagarCompra={props.onClickPagarCompra}
                    onClickAgregarMasProductos={props.onClickAgregarMasProductos}
                />}
            </>
            }
            {
                error &&
                <div
                    style={{ height: vistaReducida ? '100%' : 'calc(100vh - 192px)' }}
                    className='d-flex justify-content-center align-items-center flex-column'>
                    Error - No se pudo cargar el carrito
                </div>
            }
            {
                loading &&
                <div
                    style={{ height: vistaReducida ? '100%' : 'calc(100vh - 192px)' }}
                    className='d-flex justify-content-center align-items-center flex-column'
                >
                    <div className='carritoTextoBuscando'>Buscando carrito...</div>
                    <img src="http://localhost:8083/cart_loading.gif" alt="" />
                </div>
            }
        </>
    );
}

export default App;