import React, { useEffect, useState } from 'react';
import { Alert, Container } from "react-bootstrap";
import { ProductoCarrito } from "../App.types";
import CarritoChico from "./CarritoChico";
import ProductoCard from "./ProductoCard";

import 'bootstrap/dist/css/bootstrap.min.css';
import ProductoCardTicket from "./ProductoCardTicket";

const ProductosEnCarrito = (
    props: {
        productos: Array<ProductoCarrito>,
        setProductos: Function,
        vistaReducida?: boolean,
    }
): JSX.Element => {

    const costoTotal = (): number => {
        let total: number = 0;
        props.productos.forEach((item: ProductoCarrito) => total += item.precio * item.cantidad);
        return total;
    };
    const [vacio, setVacio] = useState<boolean>(false);

    useEffect(() => {
        setVacio(props.productos.length === 0)
    }, [props.productos]);

    const vistaReducida: boolean = props.vistaReducida ?? false;

    const usarVista = (producto: ProductoCarrito): JSX.Element => {
        return vistaReducida ?
            <ProductoCardTicket
                key={producto.id}
                productoCarrito={producto}
                setProductos={props.setProductos}
            />
            :
            <ProductoCard
                key={producto.id}
                productoCarrito={producto}
                setProductos={props.setProductos}
            />
    };

    const estaVacio = (): JSX.Element => {
        return (
            <Container fluid='xxl' className='text-center'>
                <CarritoChico size={64} />
                <h3>El carrito esta vacío.</h3>
            </Container>
        )
    };

    const tieneProductos = () => {
        return props.productos.map((item: ProductoCarrito) => usarVista(item))
    };

    const mostrarTotal = (): JSX.Element => {
        return (
            <>
                {props.vistaReducida ?
                    <span className='container-fluid d-flex justify-content-end fw-bold text-success'>
                        Total: $ {costoTotal()}
                    </span>
                    :
                    <Alert variant='success' className='mt-2 text-center'>
                        <CarritoChico size={48} />
                        <h1>Total: $ {costoTotal()}</h1>
                    </Alert>
                }
            </>
        );
    };

    return (
        <>
            <div className='d-flex flex-wrap'>
                {vacio ? estaVacio() : tieneProductos()}
            </div >
            {!vacio && mostrarTotal()}
        </>
    );
};

export default ProductosEnCarrito;