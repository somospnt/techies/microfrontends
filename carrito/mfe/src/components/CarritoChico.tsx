
import React from 'react';
import { getCarrito } from '../utils';
import IconoCarritoVacio, { IconoCarritoVacioType } from './IconoCarritoVacio';
import IconoCarritoLleno, { IconoCarritoLlenoType } from './IconoCarritoLleno';

const CarritoChico = (
    props: {
        size?: number,
        emptyColor?: string,
        notEmptyColor?: string,
    }
) => {
    const productos = getCarrito();
    const size: number = props?.size ?? 64;
    const vacio: boolean = productos.length === 0;
    const color: string = vacio ?
        props.emptyColor ? props.emptyColor : "#0d6efd"
        :
        props.notEmptyColor ? props.notEmptyColor : "#198754";

    const svgProps: IconoCarritoVacioType | IconoCarritoLlenoType = {
        size,
        color: color,
    };

    return (
        vacio ? <IconoCarritoVacio {...svgProps} /> : <IconoCarritoLleno {...svgProps} />
    );
};

export default CarritoChico;