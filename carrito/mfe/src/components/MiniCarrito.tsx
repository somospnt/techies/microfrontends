import React, { MouseEventHandler } from 'react';
import Carrito from '../CarritoMain';

import '../styles/MiniCarrito.css';

const MiniCarrito = (
    props: {
        carrito: any,
        onClickClose: Function,
        onClickIrAlCarrito: Function,
        setCarrito: Function
    }
): JSX.Element => {
    return (
        <div className='containerMiniCarrito'>
            <div className='subContainer'>
                <div>
                    <button className='botonMiniCarrito' onClick={props.onClickClose as MouseEventHandler}>Cerrar</button>
                    <button className='botonMiniCarrito' onClick={props.onClickIrAlCarrito as MouseEventHandler}>Ir al Carrito</button>
                </div >
                <Carrito vistaReducida={true} productos={props.carrito} setProductos={props.setCarrito} />
            </div >
        </div >
    );
};

export default MiniCarrito;