import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import { ProductoCarrito } from '../App.types';
import CarritoChico from './CarritoChico';
import MiniCarrito from './MiniCarrito';

import '../styles/VistaPreviaCarrito.css';

function VistaPreviaCarrito(props: {
    carrito: Array<ProductoCarrito>,
    setCarrito: Function
}) {
    const [showMini, setShowMini] = useState<boolean>(false);
    const navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        setShowMini(false)
    }, [location]);

    return (
        location.pathname !== '/carrito' &&
        <>
            <button className='iconoCarrito' onClick={() => setShowMini(prev => !prev)}>
                <CarritoChico size={32} />
            </button>
            {
                showMini &&
                <MiniCarrito
                    carrito={props.carrito}
                    onClickClose={() => setShowMini(false)}
                    onClickIrAlCarrito={() => navigate('/carrito')}
                    setCarrito={props.setCarrito} />
            }
        </>
    )
}

export default VistaPreviaCarrito;