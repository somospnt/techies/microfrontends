import React from 'react';
import { ProductoCarrito } from "../App.types";

const ProductoCardTicket = (
    props: {
        productoCarrito: ProductoCarrito,
        setProductos: Function
    }
): JSX.Element => {

    const { nombre, precio, cantidad } = props.productoCarrito;
    const subTotal = cantidad * precio;
    const infoSubTotal = `$${subTotal}`

    return (
        <div className="container-fluid">
            <div className="font-monospace">
                <div className="row border-bottom">
                    <div className="col col-1 col-sm-1 col-md-1 fs-6 text-end">
                        {cantidad}
                    </div>
                    <div className="col col-7 col-sm-8 col-md-9 fs-6 text-start">
                        {nombre}
                    </div>
                    <div className="col col-4 col-sm-3 col-md-2 fs-6 text-end fw-bold">
                        {infoSubTotal}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductoCardTicket;