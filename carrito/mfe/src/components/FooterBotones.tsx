import React from 'react';
import { Button, Col, Container, Row } from "react-bootstrap";
import { ProductoCarrito } from "../App.types";
import { clearCarrito } from "../utils";

const FooterBotones = (
    props: {
        setProductos: Function,
        productos: Array<ProductoCarrito>,
        onClickAgregarMasProductos?: Function,
        onClickVaciarCarrito?: Function,
        onClickPagarCompra?: Function,
    }
) => {

    const handleClickAgregarMasProductos = () => {
        if (props.onClickAgregarMasProductos) {
            props.onClickAgregarMasProductos();
        } else {
            history.go(-1);
        };
    };

    const handleClickVaciarCarrito = (): void => {
        if (props.onClickVaciarCarrito) {
            props.onClickVaciarCarrito();
        } else {
            clearCarrito();
            props.setProductos([]);
        };
    };

    const handleClickPagarCompra = () => {
        !!props.onClickPagarCompra && props.onClickPagarCompra();
    };

    const vacio: boolean = props.productos.length === 0 ?? false;

    return (
        <Container className='mt-3'>
            {vacio
                ?
                <div className="text-center">
                    <Button variant='success' onClick={handleClickAgregarMasProductos}>
                        Ver lista de productos
                    </Button>
                </div>
                :
                <>
                    <Row className='pb-2'>
                        <Col className='text-center d-grid'>
                            <Button variant='danger' onClick={() => handleClickVaciarCarrito()}>
                                Vaciar carrito
                            </Button>
                        </Col>
                        <Col className='text-center d-grid'>
                            <Button variant='success' onClick={handleClickPagarCompra}>
                                Pagar compra
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col className='d-grid gap-2'>
                            <Button variant='primary' onClick={handleClickAgregarMasProductos}>
                                Agregar mas productos
                            </Button>
                        </Col>
                    </Row>
                </>
            }
        </Container>
    );
};

export default FooterBotones;