import React from 'react';
import { Button } from 'react-bootstrap';
import { ProductoType } from '../App.types';
import { addCarrito } from '../utils';

type BotonCarritoProps = { producto: ProductoType, setCarrito: Function };

function BotonCarrito({ producto, setCarrito }: BotonCarritoProps) {
    return <Button onClick={() => addCarrito(producto, setCarrito)}>Agregar al Carrito</Button>
}

export default BotonCarrito;