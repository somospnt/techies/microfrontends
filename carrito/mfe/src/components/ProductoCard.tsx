import React, { useRef } from 'react';
import { ProductoCarrito } from "../App.types";
import { Button, Card, Col, Container, Row } from 'react-bootstrap';
import { addCarrito, remCarrito } from "../utils";

import '../styles/ProductoCard.css';

const ProductoCard = (
    props: {
        productoCarrito: ProductoCarrito,
        setProductos: Function
    }
): JSX.Element => {

    const { nombre, descripcion, precio, imagenUrl, cantidad } = props.productoCarrito;
    const subTotal = cantidad * precio;

    const handleClickAgregar = (producto: ProductoCarrito): void => {
        addCarrito(producto, props.setProductos);
    };
    const handleClickQuitar = (producto: ProductoCarrito): void => {
        remCarrito(producto, props.setProductos);
    };

    const imgCarrito = useRef({} as any);
    const mostrarImagenDeError = () => imgCarrito.current.src = 'http://localhost:8083/default.jpg';

    return (
        <Container>
            <Card className='cardShadow mb-4 rounded-4 overflow-hidden' >
                <Row className='justify-content-center' >
                    <Col xs="auto" className=' col-auto justify-content-center'>
                        <Card.Img
                            variant="top"
                            style={{ width: '200px' }}
                            ref={imgCarrito}
                            src={imagenUrl}
                            onError={mostrarImagenDeError}
                        />
                    </Col>
                    <Col className='d-flex flex-column justify-content-around m-4'>
                        <Row className='text-center'>
                            <Card.Title>{nombre}</Card.Title>
                        </Row>
                        <Row>
                            <Card.Text>
                                {descripcion}
                            </Card.Text>
                        </Row>
                        <Row>
                            <Col>
                                <Card.Title>
                                    $ {precio} por {cantidad} unidad/es.
                                </Card.Title>
                            </Col>
                            <Col>
                                <Card.Title>
                                    Subtotal: $ {subTotal}
                                </Card.Title>
                            </Col>
                        </Row>
                        <Row>
                            <Col className='d-flex justify-content-md-end gap-3 justify-content-center'>
                                <Button variant="primary" className='col-6 col-xxl-3' onClick={() => handleClickAgregar(props.productoCarrito)}>
                                    Agregar
                                </Button>
                                <Button variant="danger" className='col-6 col-xxl-3' onClick={() => handleClickQuitar(props.productoCarrito)}>
                                    Quitar
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Card >
        </Container >

    );
};

export default ProductoCard;