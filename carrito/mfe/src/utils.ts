import { ProductoCarrito, ProductoType } from "./App.types";

export const storage = {
    set: (key: string, val: string): void => localStorage.setItem(key, val),
    get: (key: string): string | null => localStorage.getItem(key),
    rem: (key: string): void => localStorage.removeItem(key),
    clear: (): void => localStorage.clear(),
    len: (): number => localStorage.length,
};

export const getCarrito = (): Array<ProductoCarrito> => {
    const carritoLocalStorage: string = storage.get('carrito') ?? '[]';
    let listaDeProductos: Array<ProductoCarrito> = JSON.parse(carritoLocalStorage);
    return listaDeProductos;
};

export const setCarrito = (nuevoCarrito: Array<ProductoCarrito>): void => {
    const carritoLocalStorage: string = JSON.stringify(nuevoCarrito);
    storage.set('carrito', carritoLocalStorage);
};

export const addCarrito = (producto: ProductoType, setProductos: Function): Array<ProductoCarrito> => {
    let carrito: Array<ProductoCarrito> = getCarrito();
    const indice: number = carrito.findIndex(item => item.id === producto.id);
    if (indice > -1) {
        carrito[indice].cantidad++;
    } else {
        const productoCarrito: ProductoCarrito = { ...producto, cantidad: 1 };
        carrito.push(productoCarrito);
    };
    setCarrito(carrito);
    setProductos(carrito);
    return carrito;
};

export const remCarrito = (producto: ProductoCarrito, setProductos: Function): Array<ProductoCarrito> => {
    let contenido: Array<ProductoCarrito> = getCarrito();
    const indice: number = contenido.findIndex(item => item.id === producto.id);
    if (indice > -1) {
        contenido[indice].cantidad--;
        if (contenido[indice].cantidad < 1) {
            contenido.splice(indice, 1);
        };
    };
    setCarrito(contenido);
    setProductos(contenido)
    return contenido;
};

export const clearCarrito = (): Array<ProductoCarrito> => {
    setCarrito([]);
    return [];
};