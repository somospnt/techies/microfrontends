import axios, { AxiosError, AxiosResponse } from 'axios';

import { ProductoCarrito } from '../App.types';

const services = {
    setCarrito: async () => {
        try {
            const token = localStorage.getItem('token');
            const carrito = JSON.parse(localStorage.getItem('carrito') ?? '[]');
            const response: AxiosResponse = await axios.put(
                `http://localhost:4043/api/carrito`, carrito,
                {
                    headers: {
                        authorization: `Bearer ${token}`
                    }
                });
            return response.data as Array<ProductoCarrito>;
        } catch (error: any) {
            error.origin as unknown as AxiosError;
            throw error;
        };
    },
};

export default services;