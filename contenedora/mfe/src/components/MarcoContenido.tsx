import React from 'react';
import './styles/MarcoContenido.css';

const MarcoContenido = (
    props: {
        children?: JSX.Element | JSX.Element[],
        titulo?: string,
        footer?: string,
        colorMarco?: string,
        colorTitulo?: string,
    }
): React.ReactElement => {
    const tituloMarco = props.titulo ?? 'Mi compra';
    const textoFooter = props.footer ?? '';
    const colorMarco = props.colorMarco ?? 'darkgrey';
    const colorTitulo = props.colorTitulo ?? 'grey';

    return (
        <div className='containerMarco'>
            <div
                style={{ borderColor: colorTitulo }}
                className='bordes tituloFooterMarcoBorde'>
                {
                    tituloMarco &&
                    <div
                        style={{ backgroundColor: colorTitulo }}
                        className='marcoTitulo'>
                        {tituloMarco}
                    </div>
                }
                <div
                    style={{
                        backgroundColor: colorMarco,
                        borderColor: colorTitulo
                    }}
                    className='marcoContenido bordes'>
                    {props.children}
                </div>
                {
                    textoFooter && <div
                        style={{ backgroundColor: colorTitulo }}
                        className='marcoFooter'>
                        {textoFooter}
                    </div>
                }
            </div>
        </div>
    );
};

export default MarcoContenido;