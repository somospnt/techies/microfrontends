import React, { Suspense, useState } from "react";
import { Link } from "react-router-dom";
import services from "../services/service";

const IconoDatosPersonales = React.lazy(() => import('usuario/IconoDatosPersonales'));
const CerrarSesionBoton = React.lazy(() => import('usuario/CerrarSesionBoton'));
const BotonUsuario = React.lazy(() => import('usuario/BotonUsuario'));
const VistaPreviaCarrito = React.lazy(() => import('carrito/VistaPreviaCarrito'));

import './styles/Navbar.css';
import ErrorBoundary from "./error/ErrorBoundary";
import BotonHome from "./BotonHome";

export type NavbarProps = {
    children?: any,
    carrito: any,
    setCarrito: Function,
};

const Navbar = (props: NavbarProps) => {
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const salir = () => {
        setIsLoading(true);
        services.setCarrito().then(() => setIsLoading(false));
        localStorage.removeItem('carrito');
        props.setCarrito([]);
    };

    return (
        <div className='navbarContenedora'>
            <div className='grupo1'>
                <BotonHome color="#37f" />
            </div>
            <div className='grupo2'>
                <ErrorBoundary element={<></>}>
                    <Suspense>
                        <VistaPreviaCarrito carrito={props.carrito} setCarrito={props.setCarrito} />
                    </Suspense>
                </ErrorBoundary>
                <ErrorBoundary element={<></>}>
                    <Suspense>
                        <IconoDatosPersonales color='orange' />
                        <CerrarSesionBoton onClick={salir} />
                        <BotonUsuario texto='Iniciar Sesion' path={!isLoading ? '/login' : ''} />
                        <BotonUsuario texto='Crear Cuenta' path={!isLoading ? '/nueva-cuenta' : ''} />
                    </Suspense>
                </ErrorBoundary>
            </div>
        </div >
    );
};

export default Navbar;