import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

function ModalErrorCarrito(props: { show: boolean, close: Function }) {
    const handleClose = () => props.close();

    return (
        <Modal show={props.show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Funcionalidad no disponible</Modal.Title>
            </Modal.Header>
            <Modal.Body>Estamos trabajando para solucionar el problema!!</Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleClose}>
                    Cerrar
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default ModalErrorCarrito;