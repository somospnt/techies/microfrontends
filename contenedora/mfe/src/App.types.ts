export type ProductoType = {
    id: number,
    nombre: string,
    descripcion: string,
    imagenUrl: string,
    precio: number,
    etiquetas: Array<string>,
};

export type ProductoCarrito = {
    id: number,
    nombre: string,
    descripcion: string,
    imagenUrl: string,
    precio: number,
    etiquetas: Array<string>,
    cantidad: number,
};