declare module "usuario/AuthContext";
declare module "productos/ProductosMain";
declare module "carrito/CarritoMain";
declare module "carrito/funcionesDeCarrito";
declare module "carrito/VistaPreviaCarrito";
declare module "usuario/Login";
declare module "usuario/NuevaCuenta";
declare module "usuario/DatosPersonales";
declare module "usuario/Tarjetas";
declare module "usuario/IconoDatosPersonales";
declare module "usuario/CerrarSesionBoton";
declare module "usuario/BotonUsuario";
declare module "usuario/AlertaState";
declare module "usuario/AuthState";
declare module "usuario/getUsuario";
declare module "pagos/CompraMain";
