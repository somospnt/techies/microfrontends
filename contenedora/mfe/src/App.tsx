import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';

import Contenedora from './Contenedora';
const AlertaState = React.lazy(() => import('usuario/AlertaState'));
const AuthState = React.lazy(() => import('usuario/AuthState'));
// import AlertaState from 'usuario/AlertaState';
// import AuthState from 'usuario/AuthState';
import { BrowserRouter } from 'react-router-dom';
import ErrorBoundary from './components/error/ErrorBoundary';

const root: ReactDOM.Root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <React.StrictMode>
        <ErrorBoundary element={<BrowserRouter>
            <Contenedora />
        </BrowserRouter>}>
            <Suspense>
                <AlertaState>
                    <AuthState>
                        <BrowserRouter>
                            <Contenedora />
                        </BrowserRouter>
                    </AuthState>
                </AlertaState>
            </Suspense>
        </ErrorBoundary>
    </React.StrictMode>
);