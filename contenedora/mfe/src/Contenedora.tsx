import React, { Suspense, useState, useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

import Navbar from "./components/Navbar";
import MarcoContenido from "./components/MarcoContenido";
import ErrorBoundary from "./components/error/ErrorBoundary";
import ModalErrorCarrito from "./components/error/ModalErrorCarrito";
import services from "./services/service";

const ProductosMain = React.lazy(() => import('productos/ProductosMain'));

const CompraMain = React.lazy(() => import('pagos/CompraMain'));

const Carrito = React.lazy(() => import('carrito/CarritoMain'));
const funcionesDeCarrito = import('carrito/funcionesDeCarrito');

const Login = React.lazy(() => import('usuario/Login'));
const NuevaCuenta = React.lazy(() => import('usuario/NuevaCuenta'));
const DatosPersonales = React.lazy(() => import('usuario/DatosPersonales'));
const Tarjetas = React.lazy(() => import('usuario/Tarjetas'));
const getUsuario = import('usuario/getUsuario');

const Contenedora = () => {
    const [carrito, setCarrito] = useState<any>([]);
    const [usuario, setUsuario] = useState<any>({});
    const [mostrarModal, setMostrarModal] = useState(false);
    const navigate = useNavigate();

    useEffect(() => {
        getUsuario.then(async (funcion) => {
            const data = await funcion.getUsuario();
            setUsuario(data)
        }).catch(() => { })
    }, []);

    const agregarAlCarrito = (producto: any) => {
        funcionesDeCarrito
            .then(funciones => funciones.addCarrito(producto, setCarrito))
            .catch(() => {
                setCarrito([{ ...producto, cantidad: 1 }]);
                navigate('/pagos');
            })
    };

    const pagar = () => {
        funcionesDeCarrito.then(funciones => funciones.clearCarrito());
        setCarrito([]);
        services.setCarrito();
        navigate('/');
    };

    return (
        <>
            <Navbar carrito={carrito} setCarrito={setCarrito} />
            <Routes>
                <Route path='/' element={<>
                    <MarcoContenido
                        titulo='¡Productos en stock!'
                        footer='Los mejores precios 🤑'
                        colorMarco='skyblue'
                        colorTitulo='#047'>
                        <ErrorBoundary>
                            <Suspense>
                                <ProductosMain boton={{ texto: 'Agregar al Carrito', onClick: agregarAlCarrito }} />
                            </Suspense>
                        </ErrorBoundary>
                    </MarcoContenido>
                    <ModalErrorCarrito show={mostrarModal} close={() => setMostrarModal(false)} />
                </>} />
                <Route path='/carrito' element={
                    <MarcoContenido
                        titulo='Contenido del carrito'
                        footer={carrito.length === 0
                            ? 'Aun no has comprado productos...'
                            : '¡Aun no completaste esta compra!'}
                        colorMarco='darkseagreen'
                        colorTitulo='seagreen'>
                        <ErrorBoundary>
                            <Suspense>
                                <Carrito
                                    productos={carrito}
                                    setProductos={setCarrito}
                                    onClickPagarCompra={() => navigate('/pagos')}
                                    onClickAgregarMasProductos={() => navigate('/')}
                                />
                            </Suspense>
                        </ErrorBoundary>
                    </MarcoContenido >
                } />
                < Route path='/login' element={
                    <ErrorBoundary>
                        <Suspense>
                            < Login pathToRedirect='/carrito' />
                        </Suspense>
                    </ErrorBoundary>
                } />
                < Route path='/nueva-cuenta' element={
                    <ErrorBoundary>
                        <Suspense>
                            < NuevaCuenta pathToRedirect='/' />
                        </Suspense>
                    </ErrorBoundary>
                } />
                < Route path='datos-personales' element={
                    <ErrorBoundary>
                        <Suspense>
                            < DatosPersonales pathToRedirect='/tarjetas' />
                        </Suspense>
                    </ErrorBoundary>
                } />
                < Route path='tarjetas' element={
                    <ErrorBoundary>
                        <Suspense>
                            < Tarjetas pathToRedirect='/' />
                        </Suspense>
                    </ErrorBoundary>
                } />
                < Route path='pagos' element={
                    <ErrorBoundary>
                        <Suspense>
                            <CompraMain
                                usuario={usuario}
                                pagar={pagar}
                                carrito={carrito}
                                setCarrito={setCarrito}
                                onClickPagarCompra={() => navigate('pagos')} />
                        </Suspense>
                    </ErrorBoundary>
                } />
            </Routes >
        </>
    );
};

export default Contenedora;