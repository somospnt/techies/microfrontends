import React, { useEffect, useState } from 'react';
import ListaProductos from './componentes/ListaProductos';
import { ProductoType } from './App.types';
import { listaDeProductos } from './productos.mock';

import 'bootstrap/dist/css/bootstrap.min.css';

function ProductosMain(props: { boton: { texto: string, onClick: Function } }) {
    const [productos, setProductos] = useState<Array<ProductoType>>([]);

    useEffect(() => {
        setProductos(listaDeProductos);
    }, []);

    return (
        <ListaProductos
            productos={productos}
            boton={props.boton}
        />
    );
};

export default ProductosMain;