import React from 'react';
import Button from 'react-bootstrap/Button';
import { Card } from 'react-bootstrap';
import { ProductoType } from '../App.types';

import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/Producto.css';

type ProductoProps = {
    producto: ProductoType,
    boton: { texto: string, onClick: Function },
};

function Producto({ producto, boton }: ProductoProps) {
    return (
        <Card className='cardBootstrap'>
            <div className='contenedorProducto'>
                <Card.Img variant="top" src={producto.imagenUrl} />
                <Card.Body className='bodyProducto'>
                    <div>
                        <Card.Title>{producto.nombre}</Card.Title>
                        <Card.Text>
                            {producto.descripcion}
                        </Card.Text>
                        <Card.Title>${producto.precio}</Card.Title>
                    </div>
                    <div>
                        <Button onClick={() => boton.onClick(producto)}>{boton.texto}</Button>
                    </div>
                </Card.Body>
            </div>
        </Card>
    );
}

export default Producto;