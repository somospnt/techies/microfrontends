import React from 'react';
import { ProductoType } from '../App.types';
import Producto from './Producto';

import './styles/ListaProductos.css';

type ListaProductosProps = {
    productos: Array<ProductoType>,
    boton: { texto: string, onClick: Function },
};

const ListaProductos = (
    {
        productos,
        boton,
    }: ListaProductosProps
) => {
    return (
        <div className='contenedorListaProductos'>
            {productos.map(item => (
                <Producto
                    key={item.id}
                    producto={item}
                    boton={boton} />
            ))}
        </div>
    )
};

export default ListaProductos;