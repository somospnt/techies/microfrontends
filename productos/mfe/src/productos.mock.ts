import { ProductoType } from "./App.types";

export const listaDeProductos: Array<ProductoType> = [
    {
        "id": 1,
        "nombre": "NAVARRO CORREAS",
        "descripcion": "Navarro Correas Extra Brut 750ml",
        "precio": 75,
        "imagenUrl": "http://localhost:8082/img/producto (1).jpg",
        "etiquetas": [
            "bebida", "vino", "espumante", "botella"
        ]
    },
    {
        "id": 2,
        "nombre": "Auricurales",
        "descripcion": "Auriculares Logitech H390 negro",
        "precio": 6000,
        "imagenUrl": "http://localhost:8082/img/producto (2).jpg",
        "etiquetas": [
            "electronica", "audio y video", "audio", "auriculares"
        ]
    },
    {
        "id": 3,
        "nombre": "Remera",
        "descripcion": "Remera Personalizada 100% Algodón Estampado Dtg Fotos/imagen",
        "precio": 3800,
        "imagenUrl": "http://localhost:8082/img/producto (3).jpg",
        "etiquetas": [
            "ropa", "accesorios", "remeras", "musculosas", "chombas"
        ]
    },
    {
        "id": 4,
        "nombre": "Horno empotrable",
        "descripcion": "Horno empotrable eléctrico Atma CHE3061M 70L",
        "precio": 3800,
        "imagenUrl": "http://localhost:8082/img/producto (4).jpg",
        "etiquetas": [
            "electrodomesticos", "horno", "coccion"
        ]
    },
    {
        "id": 5,
        "nombre": "Aspiradora",
        "descripcion": "Aspiradora Electrolux Ergorápido ERG21 460ml blanca 100V/240V",
        "precio": 52,
        "imagenUrl": "http://localhost:8082/img/producto (5).jpg",
        "etiquetas": [
            "electrodomesticos", "hogar", "aspiradoras"
        ]
    },
    {
        "id": 6,
        "nombre": "Notebook",
        "descripcion": 'Notebook Enova gris 14", Intel Core i5 8GB de RAM 480GB SSD',
        "precio": 229,
        "imagenUrl": "http://localhost:8082/img/producto (6).jpg",
        "etiquetas": [
            "computacion", "laptops", "notebooks"
        ]
    },
    {
        "id": 7,
        "nombre": "Helado sándwich de nata",
        "imagenUrl": "http://localhost:8082/img/sandwich-nata.jpg",
        "descripcion": "Caja 6 ud. (600 ml)",
        "precio": 2,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 8,
        "nombre": "Helado cucurucho de vainilla",
        "imagenUrl": "http://localhost:8082/img/cucuruchos-vainilla.jpg",
        "descripcion": "Caja 6 ud. (720 ml)",
        "precio": 2,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 9,
        "nombre": "Mini surtido",
        "imagenUrl": "http://localhost:8082/img/mini-surtido.jpg",
        "descripcion": "Caja 6 ud. (720 ml)",
        "precio": 2,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 10,
        "nombre": "Helado mini doble sándwich nata",
        "imagenUrl": "http://localhost:8082/img/sandwich-doble.jpg",
        "descripcion": "Caja 6 ud. (480 ml)",
        "precio": 2,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 11,
        "nombre": "Helado de fresa",
        "imagenUrl": "http://localhost:8082/img/fresa-hielo.jpg",
        "descripcion": "Caja 10 ud. (750 ml)",
        "precio": 2,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 12,
        "nombre": "Helado yogoice Mr. Toddy",
        "imagenUrl": "http://localhost:8082/img/yogoice.jpg",
        "descripcion": "Caja 8 ud. (450 ml)",
        "precio": 2,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 13,
        "nombre": "Helado sabor huevo de chocolate",
        "imagenUrl": "http://localhost:8082/img/huevo.jpg",
        "descripcion": "Tarrina 1 L.",
        "precio": 3,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 14,
        "nombre": "Medios cocos helados",
        "imagenUrl": "http://localhost:8082/img/medio-coco-helado.jpg",
        "descripcion": "Caja 2 ud. (350 ml)",
        "precio": 4,
        "etiquetas": [
            "helado"
        ]
    },
    {
        "id": 15,
        "nombre": "Agua minaral",
        "descripcion": "Agua mineralizada sin gas por 1 litro en botella plástica no retornable.",
        "precio": 15,
        "imagenUrl": "https://cadenaser.com/resizer/6_cLUpzDRiOs99PDN8Gnc2pgL7I=/1200x900/filters:format(jpg):quality(70)/cloudfront-eu-central-1.images.arcpublishing.com/prisaradio/ZGIOCMP7FZOZRO7LKNC7CMWP7Y.jpg",
        "etiquetas": [
            "bebida", "hidratar", "mineral", "agua", "botella", "plastico"
        ]
    },
    {
        "id": 16,
        "nombre": "Coca-cola",
        "descripcion": "Gaseosa cola por 3 litros en botella de vidrio retornable.",
        "precio": 40,
        "imagenUrl": "https://carrefourar.vtexassets.com/arquivos/ids/220177/7790895000997_02.jpg?v=637704294205400000",
        "etiquetas": [
            "bebida", "hidratar", "gaseosa", "botella", "retornable"
        ]
    },
    {
        id: 17,
        nombre: "Agua mineral",
        descripcion: "Botella de agua 1 litro.",
        imagenUrl: "https://cdn.shopify.com/s/files/1/0254/2947/5433/products/agua-eco-andes-pet-2000-siempreencasa_500x.png?v=1633359956?nocache=0.11452011740766466",
        precio: 12,
        etiquetas: [
            "agua", "botella"
        ],
    },
    {
        id: 18,
        nombre: "Coca-cola",
        descripcion: "Botella de Coca-cola 1 litro.",
        imagenUrl: "https://jumboargentina.vtexassets.com/arquivos/ids/441468/Coca-Cola-25-L-3-17483.jpg?v=636528846231600000",
        precio: 20,
        etiquetas: [
            "gaseosa", "botella", "bebida"
        ],
    },
    {
        id: 19,
        nombre: "Cerveza Quilmes",
        descripcion: "Botella de cerveza 1 litro.",
        imagenUrl: "https://mateenmano.com/wp-content/uploads/sites/15/2020/10/quilmes.jpg",
        precio: 17,
        etiquetas: [
            "cerveza", "botella", "bebida"
        ],
    },
    {
        id: 20,
        nombre: "Remera",
        descripcion: "Remera lisa color blanco talle L.",
        imagenUrl: "https://http2.mlstatic.com/D_NQ_NP_833325-MLA25414712525_032017-W.jpg",
        precio: 33,
        etiquetas: [
            "ropa", "blanco", "L", "deporte"
        ],
    },
    {
        id: 21,
        nombre: "Pelota de futbol",
        descripcion: "Pelota de futbol numero 5.",
        imagenUrl: "https://www.deportesmd.com.ar/sistema/uploads/699/articulos/pelota-de-futbol-n-5-campo-argentina-pro.jpg",
        precio: 40,
        etiquetas: [
            "deporte", "pelota", "futbol"
        ],
    },
    {
        id: 22,
        nombre: "Pack 3 pelotas de tenis",
        descripcion: "Pelota de tenis por 3 unidades.",
        imagenUrl: "https://cdn.solodeportes.com.ar/media/catalog/product/cache/7c4f9b393f0b8cb75f2b74fe5e9e52aa/7/1/710040075203001-1.jpg",
        precio: 36,
        etiquetas: [
            "deporte", "pelota", "tenis"
        ],
    }
];