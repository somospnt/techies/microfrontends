import React from 'react';
import ReactDOM from 'react-dom/client';
import { ProductoType } from './App.types';
import ProductosMain from './ProductosMain';

const root: ReactDOM.Root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

const agregarAlCarrito = (producto: ProductoType) => {
  let carrito = JSON.parse(window.localStorage.getItem('carrito') || '[]');
  if (!carrito) {
    carrito = [{ ...producto, cantidad: 1 }];
  } else {
    const indice: number = carrito.findIndex((item: ProductoType) => item.id === producto.id);
    if (indice > -1) {
      carrito[indice].cantidad++;
    } else {
      carrito = [...carrito, { ...producto, cantidad: 1 }];
    };
  };
  localStorage.setItem('carrito', JSON.stringify(carrito));
};

root.render(
  <React.StrictMode>
    <ProductosMain boton={{ texto: 'Agregar al Carrito', onClick: agregarAlCarrito }} />
  </React.StrictMode>
);